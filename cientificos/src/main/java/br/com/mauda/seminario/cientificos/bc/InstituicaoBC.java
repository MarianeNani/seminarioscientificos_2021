package br.com.mauda.seminario.cientificos.bc;

public class InstituicaoBC extends PatternCrudBC<Instituicao> {

    private static InstituicaoBC instance = new InstituicaoBC();

    private InstituicaoBC() {
        //nada a ser realizado
    }

    public static InstituicaoBC getInstance() {
        return instance;
    }

}