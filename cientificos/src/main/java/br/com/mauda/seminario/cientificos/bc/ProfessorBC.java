package br.com.mauda.seminario.cientificos.bc;

public class ProfessorBC extends PatternCrudBC<Professor> {

    private static ProfessorBC instance = new ProfessorBC();

    private ProfessorBC() {
        //nada a ser realizado
    }

    public static ProfessorBC getInstance() {
        return instance;
    }

}