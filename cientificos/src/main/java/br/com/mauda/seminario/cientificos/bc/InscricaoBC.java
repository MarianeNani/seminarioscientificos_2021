package br.com.mauda.seminario.cientificos.bc;

public class InscricaoBC extends PatternCrudBC<Inscricao> {

    private static InscricaoBC instance = new InscricaoBC();

    private InscricaoBC() {
        //nada a ser realizado
    }

    public static InscricaoBC getInstance() {
        return instance;
    }

}