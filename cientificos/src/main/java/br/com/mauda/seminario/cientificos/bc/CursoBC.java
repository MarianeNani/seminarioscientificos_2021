package br.com.mauda.seminario.cientificos.bc;

public class CursoBC extends PatternCrudBC<Curso> {

    private static CursoBC instance = new CursoBC();

    private CursoBC() {
        //nada a ser realizado
    }

    public static CursoBC getInstance() {
        return instance;
    }
}