package br.com.mauda.seminario.cientificos.bc;

public class EstudanteBC extends PatternCrudBC<Estudante> {

    private static EstudanteBC instance = new EstudanteBC();

    private EstudanteBC() {
        //nada a ser realizado
    }

    public static EstudanteBC getInstance() {
        return instance;
    }
}