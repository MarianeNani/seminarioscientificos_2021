package br.com.mauda.seminario.cientificos.bc;

public class AreaCientificaBC extends PatternCrudBC<AreaCientifica> {

    private static AreaCientificaBC instance = new AreaCientificaBC();

    private AreaCientificaBC() {
        //nada a ser realizado
    }

    public static AreaCientificaBC getInstance() {
        return instance;
    }

}