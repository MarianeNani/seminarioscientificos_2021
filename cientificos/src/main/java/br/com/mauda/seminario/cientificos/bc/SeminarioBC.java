package br.com.mauda.seminario.cientificos.bc;

public class SeminarioBC extends PatternCrudBC<Seminario> {

    private static SeminarioBC instance = new SeminarioBC();

    private SeminarioBC() {
        //nada a ser realizado
    }

    public static SeminarioBC getInstance() {
        return instance;
    }

}