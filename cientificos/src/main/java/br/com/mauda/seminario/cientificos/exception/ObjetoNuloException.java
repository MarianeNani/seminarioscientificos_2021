package br.com.mauda.seminario.cientificos.exception;

/**
 * Classe de Exception para o projeto de Seminarios Cientificos
 *
 * @author Mauda
 *
 */

public class ObjetoNuloException extends RuntimeException {

    private static final long serialVersionUID = 4928599035264976611L;

    public ObjetoNuloException() {
        super("ER003");
    }

    public ObjetoNuloException(Throwable t) {
        super(t);
    }
}
