package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica implements DataValidation {

    private Long id;
    private String nome;
    private List<Curso> cursos;

    public AreaCientifica() {
        super();
        this.cursos = new ArrayList<>();
    }

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void validateForDataModification() {        
        if(StringUtils.isBlank(this.nome); || this.name.length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }

        if(this.cursos == null) {
            throw new ObjetoNuloException();
        }

        for(Curso curso : this.cursos) {
            if(curso == null) {
                throw new ObjetoNuloException();
            }
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        AreaCientifica other = (AreaCientifica) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
