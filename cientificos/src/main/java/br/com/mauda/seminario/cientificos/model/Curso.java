package br.com.mauda.seminario.cientificos.model;

public class Curso implements DataValidation {

    private Long id;
    private String nome;
    private AreaCientifica areaCientifica;

    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void validateForDataModification() {

        if(StringUtils.isBlank(this.nome); || this.name.length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }

        if(this.areaCientifica == null) {
            throw new ObjetoNuloException();
        }

        this.areaCientifica.validateForDataModification();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Curso other = (Curso) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
